#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h" 
#include "ros/time.h"
#include "geometry_msgs/WrenchStamped.h"
#include <iostream>
#include "rgb_calib_msgs/RgbCalibImg.h"
#include "std_msgs/Float64MultiArray.h"

//void Callback_state(const std_msgs::Float64::ConstPtr & msg);

rgb_calib_msgs::RgbCalibImg global_msg_image_state_force;
int is_state_received, is_force_received, is_image_received;

void Callback_state(const std_msgs::Float64 msg);
void Callback_optoforce(const geometry_msgs::WrenchStamped msg);
void Callback_camera(sensor_msgs::Image msg);
void Callback_feature(const std_msgs::Float64MultiArray msg);

//double s_temp;
//geometry_msgs::WrenchStamped o_temp;

//int x = 100000;


int main(int argc, char **argv)
{
  //printf(" \n\n Publishing states, optoforce, camera and feature value in 1 topic ");
  
  ros::init(argc, argv, "subscribe_state");  
  ros::NodeHandle n;
  ros::Subscriber sub_state = n.subscribe("/current_state", 100, Callback_state);
  ros::Subscriber sub_opto = n.subscribe("/optoforce_0", 100, Callback_optoforce);
  ros::Subscriber sub_camera = n.subscribe("/camera/image_raw", 100, Callback_camera);
  ros::Subscriber sub_feature = n.subscribe("/image_feature", 100, Callback_feature);

  ros::Publisher all_pub = n.advertise<rgb_calib_msgs::RgbCalibImg>("/all_data_calib", 2);
  //ros::Publisher all_pub = n.advertise<const Integer< int > >("/all_data_calib", 100);
  ros::spinOnce();
  ros::Rate loop_rate(125);
  
  while (ros::ok())
  {
     //std_msgs::String msg;
     //std::stringstream ss;
     //msg.data = ss.str();
     global_msg_image_state_force.header.stamp = ros::Time::now();
     //if ((is_state_received == 1)&&(is_force_received==1)&& (is_image_received ==1))
     //{
	all_pub.publish(global_msg_image_state_force);
	ros::spinOnce();
	is_state_received = 0;
	is_force_received = 0;
	is_image_received = 0;

     //}
     
     ros::spinOnce();	

     //global_msg_image_state_force.own_image.data.clear();
	

     loop_rate.sleep();
  }
  
return 0;
}

void Callback_state(const std_msgs::Float64 msg)
{
	is_state_received = 1;
	global_msg_image_state_force.states = msg.data;
	//Book.s_temp = msg.data;
	//ROS_INFO("I heard: [%f]", data_mine::s_temp);
	//ROS_INFO_STREAM(data_mine::s_temp);
}



void Callback_optoforce(const geometry_msgs::WrenchStamped msg)
{
	is_force_received = 1;
	global_msg_image_state_force.fwx = msg.wrench.torque.x;
	global_msg_image_state_force.fwy = msg.wrench.torque.y;
	global_msg_image_state_force.fwz = msg.wrench.torque.z;
	global_msg_image_state_force.fx = msg.wrench.force.x;
	global_msg_image_state_force.fy = msg.wrench.force.y;
	global_msg_image_state_force.fz = msg.wrench.force.z;

	//Book.o_temp = msg;
	geometry_msgs::WrenchStamped temp = msg;
	//uint32_t temp2 = msg.header.seq;
	//geometry_msgs::WrenchStamped temp3 = msg.header.stamp;
	//ROS_INFO("I heard: %lf", temp.wrench.force.z);
// 	ROS_INFO_STREAM(data_mine::o_temp);

}

void Callback_camera(sensor_msgs::Image msg){
  is_image_received = 1;
  global_msg_image_state_force.own_image.height = msg.height;
  global_msg_image_state_force.own_image.width = msg.width;
  global_msg_image_state_force.own_image.data = msg.data;
  global_msg_image_state_force.own_image.header = msg.header;
  global_msg_image_state_force.own_image.encoding = msg.encoding;
  global_msg_image_state_force.own_image.is_bigendian = msg.is_bigendian;
  global_msg_image_state_force.own_image.step = msg.step;
}



void Callback_feature(const std_msgs::Float64MultiArray msg)
{
	is_state_received = 1;
	
	global_msg_image_state_force.feature.data = msg.data;
	
	//Book.s_temp = msg.data;
	//ROS_INFO("I heard: [%f]", data_mine::s_temp);
	//ROS_INFO_STREAM(data_mine::s_temp);
}

  
