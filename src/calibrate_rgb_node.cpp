#include <ros/ros.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PointStamped.h>
#include <sensor_msgs/Image.h>
#include <opencv2/core/core.hpp>
#include <image_transport/image_transport.h> 
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
class CalibRGBsensor

{
public:
  CalibRGBsensor();
  ~CalibRGBsensor();
  
  ros::Subscriber force;
  void callback_force(const geometry_msgs::WrenchStamped& msg);
  float fx, fy, fz, mx, my, mz;
    
  ros::Subscriber pose_now;
  void callback_posenow(const geometry_msgs::PoseStamped& msg);
  float x, y, z, wx, wy, wz, ww;

  ros::Subscriber contacts_hist;
//   void callback_contact_hist(const geometry_msgs::PointStamped& msg);
  float xc, yc, zc;
  
  long double t_now_sec, t_begin_sec;
  long double t_now_nsec;
  ros::Duration dt, test_time;
  
  ros::Time t_old_sec_total, test_time_begin;
  long double n_cycle;
  long double test_time2;
  
  bool init_time;

  ros::NodeHandle nh;
  ros::Publisher pub_plot_calib_rgb;
  
  bool is_feat_fb_rec_, is_feat_des_rec_,
  is_feat_plan_rec_, is_ati_rec_, is_pose_now_rec_,
  is_pose_des_rec_, is_cont_hist_rec_;
  
  void send_plot();

  ros::Time the_time;
  
  image_transport::ImageTransport it_viz;
  image_transport::Subscriber  img_now;
  void callback_img_now(const sensor_msgs::ImageConstPtr& msg);
  std::vector<float> img;
bool is_img_rec;
bool is_img_sent;
};

CalibRGBsensor::CalibRGBsensor():it_viz(nh)
{
  force = nh.subscribe("/optoforce_0", 10, &CalibRGBsensor::callback_force, this);
   
  pose_now = nh.subscribe("/pose_current", 10,  &CalibRGBsensor::callback_posenow,this);
  
  
//   contacts_hist = nh.subscribe("/contact_points_weiss_plot", 10,  &CalibRGBsensor::callback_contact_hist,this);
  
  pub_plot_calib_rgb = nh.advertise<geometry_msgs::WrenchStamped>("/calib_rgb_topic", 10);
  
  is_feat_fb_rec_ = false;
  is_feat_des_rec_ = false;
  is_feat_plan_rec_ = false;
  is_ati_rec_ = false;
  is_pose_now_rec_ = false;
  is_pose_des_rec_ = false;
  is_cont_hist_rec_ =false;
  init_time = false;
  
  n_cycle = 0;
  
  img_now = it_viz.subscribe("/camera/image_raw", 1, &CalibRGBsensor::callback_img_now, this);
  is_img_rec = 0;
  is_img_sent = 0;
}
CalibRGBsensor::~CalibRGBsensor()
{}

void CalibRGBsensor::callback_img_now(const sensor_msgs::ImageConstPtr& msg)
{
//   int rows_ = 14;
//   int cols_ = 6;
  cv_bridge::CvImagePtr cv_ptr;
  try
  {
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC3);//TYPE_8UC1
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  
  if (is_img_sent == 1)
  {  
//   int c = 0;
//   for(int i=0; i<rows_; ++i) {
//     for(int j=0; j<cols_; j++) {
//       // make it visible
//       img.push_back(cv_ptr->image.data[c] * 3);
//       c = c + 1;
//     }
//   }
  is_img_rec = 1;
  is_img_sent = 0;
  }
}
  
 
void CalibRGBsensor::callback_force(const geometry_msgs::WrenchStamped& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
  fx = msg.wrench.force.x;
  fy = msg.wrench.force.y;
  fz = msg.wrench.force.z;
  mx = msg.wrench.torque.x;
  my = msg.wrench.torque.y;
  mz = msg.wrench.torque.z;
  is_ati_rec_ = true;
}

void CalibRGBsensor::callback_posenow(const geometry_msgs::PoseStamped& msg)
{
  x = msg.pose.position.x;
  y = msg.pose.position.y;
  z = msg.pose.position.z;
  wx = msg.pose.orientation.x;
  wy = msg.pose.orientation.y;
  wz = msg.pose.orientation.z;
  ww = msg.pose.orientation.w;
  is_pose_now_rec_ = true;
}


void CalibRGBsensor::send_plot()
{
//    if ( is_feat_fb_rec_ && is_feat_des_rec_ && is_pose_des_rec_ )
//   {
    geometry_msgs::WrenchStamped plot_matl;
    plot_matl.header.frame_id = "calib_rgb_optoforce";
    plot_matl.header.stamp = ros::Time::now();
    
    plot_matl.wrench.force.x = fx;
    plot_matl.wrench.force.y = fy;
    plot_matl.wrench.force.z = fz;
    
    if (is_img_rec)
    {
//       for (int ind = 0; ind < img.size(); ind++)
//       {
// 	plot_matl.img.push_back(img.at(ind));
//       }
       is_img_rec = 0;
//       img.clear();
    }
    
    if (!init_time)
    {
      test_time_begin = ros::Time::now();
    }
    // correct time
    test_time = ros::Time::now() - test_time_begin;
    
    if(init_time)
    {
      dt = ros::Time::now() - t_old_sec_total;
    }
    
    t_old_sec_total = ros::Time::now();
    
    if(init_time)
    {
      n_cycle = n_cycle + 1;
//       plot_matl.dt = dt.toSec();
      test_time2 = dt.toSec() * n_cycle;
      // not correct time as the dt is not stable
//       plot_matl.test_time2 = test_time2;
    }
    
    init_time = true;
    
//     plot_matl.time = test_time.toSec();
    
    ROS_DEBUG_STREAM("all_recieved");
    pub_plot_calib_rgb.publish(plot_matl);
    is_feat_des_rec_ = false;
    is_feat_fb_rec_ = false;
    is_pose_now_rec_ = false;
    is_pose_des_rec_ = false;
    
//     img.clear();
//     plot_matl.img.clear();
    is_img_sent = 1;
//   }
  
}
int main(int argc, char** argv)
{
    ros::init(argc, argv, "calib_rgb_node");
    ros::NodeHandle n;
    ros::Rate loop_rate(100);
    ROS_INFO("calib_rgb_node");
    CalibRGBsensor plot_matlab;
    while( ros::ok() )
    {
      plot_matlab.send_plot();
      ros::spinOnce();
      loop_rate.sleep();
    }
    return 0;
}


